import { useEffect, useState } from 'react'

export function LazyLoadMarker(props: {
  loadMore: () => void
  disabled: boolean
}) {
  const [div, setDiv] = useState<HTMLDivElement | null>(null)
  useEffect(() => {
    if (!div) {
      return
    }
    let observer = new IntersectionObserver(entries => {
      entries.forEach(entry => {
        if (!entry.isIntersecting) {
          return
        }
        // console.log(entry)
        const isBottom =
          entry.intersectionRect.bottom === entry.rootBounds?.height
        // console.log({ isBottom })
        if (isBottom) {
          props.loadMore()
        }
      })
    })
    observer.observe(div)
    return () => {
      observer.unobserve(div)
      observer.disconnect()
    }
  }, [div, props.loadMore])
  return (
    <div
      ref={e => {
        if (e !== div) {
          setDiv(e)
        }
      }}
    >
      load more?....
    </div>
  )
}
