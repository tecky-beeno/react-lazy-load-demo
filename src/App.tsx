import React, { useCallback, useEffect, useState } from 'react'
import logo from './logo.svg'
import './App.css'
import { useGet } from './hooks/use-get'
import { LazyLoadMarker } from './LazyLoadMarker'

type ShopListResponse = {
  error: string
  list: Shop[]
  total: number
}

type CategoryListResponse = {
  error: string
  list: { id: number; name: string }[]
}

type Shop = {
  id: number
  name: string
  cat_id: number
  order: number
  image: string
}

const itemPerPage = 10

function App() {
  const [cat, setCat] = useState<number | 'all'>('all')

  //  shop id -> shop
  const [allShops, setAllShops] = useState<Record<number, Shop>>({})
  // cat id -> shop id -> shop
  const [allShopsByCat, setAllShopsByCat] = useState<
    Record<number, Record<number, Shop>>
  >({})

  const catList = useGet<CategoryListResponse>(
    '/cats',
    {
      error: 'loading',
      list: [],
    },
    () => ({
      error: '',
      list: [
        { id: 1, name: 'apple' },
        { id: 2, name: 'banana' },
        { id: 3, name: 'cherry' },
      ],
    }),
  )

  const [offset, setOffset] = useState(0)

  useEffect(() => {
    setOffset(0)
  }, [cat])

  const params = new URLSearchParams()
  params.set('cat', String(cat))
  params.set('offset', String(offset))
  params.set('limit', String(itemPerPage))
  const shopList = useGet<ShopListResponse>(
    '/shops?' + params,
    {
      error: 'loading',
      list: [],
      total: 100,
    },
    () => ({
      error: '',
      list: new Array(itemPerPage).fill(0).map((_, i) => {
        let id = offset + i
        return {
          id,
          name: 'shop ' + id,
          cat_id: 1,
          image: `https://picsum.photos/id/${id}/200/300`,
          order: 1,
        }
      }),
      total: 100,
    }),
  )

  const hasMore = shopList.state.total >= offset + itemPerPage

  const loadMore = useCallback(() => {
    if (!hasMore) return
    setOffset(offset => offset + itemPerPage)
  }, [hasMore])

  useEffect(() => {
    if (!shopList.state.list) {
      return
    }

    let newAllShops: Record<number, Shop> = allShops
    let newAllShopsByCat: Record<number, Record<number, Shop>> = allShopsByCat

    shopList.state.list.forEach(shop => {
      newAllShops = { ...newAllShops, [shop.id]: shop }

      let byCat = newAllShopsByCat[shop.cat_id] || {}
      byCat = { ...byCat, [shop.id]: shop }
      newAllShopsByCat = { ...newAllShopsByCat, [shop.cat_id]: byCat }
    })

    setAllShops(newAllShops)
    setAllShopsByCat(newAllShopsByCat)
  }, [shopList.state.list])

  const shopsToDisplay: Shop[] = Object.values(
    cat === 'all' ? allShops : allShopsByCat[cat],
  )
    .sort((a, b) => a.order - b.order)
    .slice(0, offset + itemPerPage)

  return (
    <div className="App">
      <select
        value={cat}
        onChange={e =>
          setCat(e.target.value === 'all' ? 'all' : +e.target.value)
        }
      >
        <option value="all">All</option>
        {catList.state.list?.map(cat => (
          <option value={cat.id} key={cat.id}>
            {cat.name}
          </option>
        ))}
      </select>

      {shopList.state.error ? (
        <p>{shopList.state.error}</p>
      ) : (
        shopsToDisplay.map((shop, i) => (
          <div key={shop.id}>
            <div>{shop.name}</div>
            <img
              src={shop.image}
              loading="lazy"
              // onLoad={() => {
              //   const isLast = i === shopsToDisplay.length - 1
              //   if (isLast) {
              //     loadMore()
              //   }
              // }}
            />
          </div>
        ))
      )}
      <LazyLoadMarker loadMore={loadMore} disabled={!hasMore} />
    </div>
  )
}

export default App
