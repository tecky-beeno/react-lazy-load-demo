import { useEffect, useState } from 'react'

let API_ORIGIN = 'http://localhost:8100'

export function useGet<T>(url: string, defaultValue: T, fakeFn: () => T) {
  const [state, setState] = useState(defaultValue)

  function download() {
    fetch(API_ORIGIN + url)
      .then(res => res.json())
      // .catch(err => ({ error: String(err) }))
      .catch(fakeFn)
      .then(setState)
  }

  useEffect(download, [url])

  return { state, reload: download, setState }
}
